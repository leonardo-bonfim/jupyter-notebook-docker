FROM jupyter/datascience-notebook
USER root

WORKDIR /home/jovyan/work/

RUN chown -R jovyan /home/jovyan/work/
RUN pip install pymongo
RUN pip install regex
RUN pip install pixiedust
RUN pip install pixiedust_node
RUN pip install elasticsearch
